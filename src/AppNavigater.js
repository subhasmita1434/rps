import React from 'react';
import { View, Text } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Login from '../src/Login';
import SignUp from '../src/SignUp'

class HomeScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Home Screen</Text>
      </View>
    );
  }
}


const AppNavigator = createStackNavigator({
   
      Login:{
          screen:Login
      },
      SignUp:{
          screen:SignUp
      }
  },
  {
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    }
   }
  );
export default createAppContainer(AppNavigator);