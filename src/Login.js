import React, { Component } from 'react';
import {
  StyleSheet,
  Image,ImageBackground,BackHandler,View,Text,TouchableOpacity,TextInput,Alert
} from 'react-native';


import {hp, wp} from '../src/Utility/size';

import Color from '../src/Constants/color';

class Login extends Component {
    
  constructor(props){
    super(props)
    this.state={
        secureTextEntry:true,
        email:'',
        password:''

    }
  }

  componentDidMount() {

   }

   ButtonClickForgetPasswordScreen = () => {
    this.props.navigation.navigate("ForgetPasswordScreen");
  }

   componentWillMount() {  
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {

  BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);

  }

handleBackButtonClick = () => {

  if (this.props.navigation.isFocused()) {
    return true;
  }    
}

login=()=>{
    if(this.state.email && this.state.password){
        if(!/^[0-9]+$/.test(this.state.email) && !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.state.email)){
            Alert.alert("Please enter the correct Mobile/email !");  
        }else{
            // api called 
        }
      }else{
          Alert.alert("Please fill all the fields !")
      }

}



  render() {

    return (
      <View style={styles.container}> 
                      <View>
                      <Image style={{marginRight:wp(80),width:30,height:30,marginTop:10}} source = {require('../src/image/back.png')}></Image>
                      </View>
                    <View>
                        <Text style={{marginTop:150,fontSize:20,fontWeight:'bold',color:'#e86100'}}>Welcome back</Text>
                    </View>
                    <View style={{marginTop:10}}>
                    <View style={styles.loginView}>
                        <TextInput style={styles.input} 
                          placeholder='Enter' 
                          keyboardType = 'email-address'
                          autoCorrect={false}
                          returnKeyType={'done'}
                          value={this.state.email}
                          onChangeText={(email) => this.setState({ email })}
                          >
                        </TextInput>
                    </View>

                    <View style={styles.loginView}>
                        <TextInput style={styles.input} 
                          placeholder='Password' 
                          value={this.state.password}
                          onChangeText={(password) => this.setState({ password })}
                          secureTextEntry={true}
                          >
                        </TextInput>
                       

                    </View>
                    </View>

                    <View style={styles.footerView}>
                        <View>
                        <TouchableOpacity onPress={ this.ButtonClickForgetPasswordScreen} >
                            <Text style={styles.forgot1}>Forgot Password?</Text>
                        </TouchableOpacity>
                        </View>

                        <TouchableOpacity onPress={()=>this.login()} style={styles.goBtn}>
                            <Text style={{fontSize:14,fontWeight:'bold',color:'white'}}>LOGIN</Text>
                        </TouchableOpacity>

                         <TouchableOpacity onPress={() => this.props.navigation.navigate('SignUp')} >
                            <Text style={styles.forgot}>Don't Have An Account? <Text style={{fontSize:12,color:'#e86100',fontWeight:'bold'}}>Sign Up</Text></Text>
                        </TouchableOpacity>

                    </View>
                  
           
       
        </View>
    )
  }
}


const styles = StyleSheet.create({
  container:{width:"100%",height:"100%",flex:1,alignItems:'center',backgroundColor:'#E9EBEE'},
    loginView:{flexDirection:'row',width:wp(80),borderWidth:1,borderRadius:6,height:50,alignItems:'center',margin:12,},
    icon:{height:22,width:22,resizeMode:'contain',margin:10},
    input:{ flex:1, padding: 8 ,fontSize:16,paddingLeft:16},
    footerView:{width:wp(100),alignItems:'center',},
    goBtn:{height:40,width:wp(80),borderRadius:5,marginTop:20,backgroundColor:'blue',alignItems:'center',justifyContent:'center'},
    forgot1:{fontSize:12,marginLeft:170},
    forgot:{fontSize:12,padding:8,},
})

export default Login