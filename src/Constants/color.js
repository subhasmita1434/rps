const Color = {
    lemonYellow: '#FCE872',
    peach: '#FFAD9F',
    darkGray: '#262626',
    red: '#EC2D2D',
    silvergray:'#a0a0a0',
    yellow:'#fac132'
 } 

 export  default Color