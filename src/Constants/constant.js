
export const LOCALES = {
    ENGLISH: { id: 1, name: 'en', label: 'ENGLISH' },
    ARABIC: { id: 2, name: 'ar', label: 'عربي' }
  }